# Copyright (C) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//developtools/profiler/build/config.gni")
import("BUILD.generated.gni")

config("libflac_config"){
  defines = []
  cflags = [
    "-D_GNU_SOURCE",
    "-D_HAS_EXCEPTIONS=0",
    "-DHAVE_CONFIG_H",
    "-Wno-macro-redefined",
  ]

  cflags_cc = [
    "-frtti"
  ]

  include_dirs = [
    "//third_party/src/libFLAC",
    "//third_party/libogg/include",
    "//third_party/libflac",
    "//third_party/libflac/include",
    "//third_party/libflac/include/FLAC",
    "//third_party/libflac/include/FLAC++",
    "//third_party/libflac/src/libFLAC/include",
    "//third_party/libflac/src/libFLAC/include",
    "//third_party/libflac/src/flac/",

  ]
}

ohos_shared_library("libFLAC"){
  sources = LIBFLAC_SOURCES
  configs = [":libflac_config",]
  deps = ["//third_party/libogg:libogg",]
  libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("libFLAC-static"){
   sources = LIBFLAC_SOURCES
   configs = [":libflac_config",]
   libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("getopt/libgetopt"){
   sources = GETOPT_SOURCES
   configs = [":libflac_config",]
   libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("grabbag/libgrabbag"){
  sources = GRABBAG_SOURCES
  configs = [":libflac_config",]
  libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("utf8/libutf8"){
  sources = UTF8_SOURCES
  configs = [":libflac_config",]
  libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("replaygain_analysis/libreplaygain_analysis"){
  sources = ["./src/share/replaygain_analysis/replaygain_analysis.c",]
  configs = [":libflac_config",]
  libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("replaygain_synthesis/libreplaygain_synthesis"){
  sources = ["./src/share/replaygain_synthesis/replaygain_synthesis.c",]
  configs = [":libflac_config",]
  libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("libtest_libs_common"){
  sources = ["./src/test_libs_common/file_utils_flac.c",
             "./src/test_libs_common/metadata_utils.c",
  ]
  configs = [":libflac_config",]
  libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("libFLAC/libFLAC-static"){
  sources = LIBFLAC_SOURCES
  configs = [":libflac_config",]
  libs = [ "m" ]
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_shared_library("libFLACpp"){
  sources = LIBFLAC_V_SOURCES
  configs = [":libflac_config",]
  deps = [":libFLAC",]
  libs = [ "m" ]
  output_name = "libFLAC++"
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_static_library("libFLACpp-static"){
  sources = LIBFLAC_V_SOURCES
  configs = [":libflac_config",]
  deps = [":libFLAC-static",]
  libs = [ "m" ]
  output_name = "libFLAC++-static"
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}


ohos_executable("flac"){
  sources = FLAC_SOURCES
  configs = [":libflac_config",]
  deps = [
    ":utf8/libutf8",
    ":grabbag/libgrabbag",
    ":getopt/libgetopt",
    ":replaygain_analysis/libreplaygain_analysis",
    ":replaygain_synthesis/libreplaygain_synthesis",
    ":libFLAC",
  ]
  libs = [ "m" ]
}

ohos_executable("metaflac"){
  sources = METAFLAC_SOURCES
  configs = [":libflac_config",]
  deps = [
    ":grabbag/libgrabbag",
    ":replaygain_analysis/libreplaygain_analysis",
    ":getopt/libgetopt",
    ":utf8/libutf8",
    ":libFLAC",
  ]
  libs = [ "m" ]
}


ohos_executable("benchmark_residual"){
  sources = BENCHMARK_RESIDUAL_SOURCES
  configs = [":libflac_config",]
  libs = [ "m",
           "rt",
  ]
}

group("libflac_targets"){
  deps = [
    ":libFLAC",
    ":libFLAC-static",
    ":getopt/libgetopt",
    ":grabbag/libgrabbag",
    ":utf8/libutf8",
    ":replaygain_analysis/libreplaygain_analysis",
    ":replaygain_synthesis/libreplaygain_synthesis",
    ":flac",
    ":metaflac",
    ":libtest_libs_common",
    ":libFLACpp",
    ":libFLACpp-static",
    ":benchmark_residual",
    #":example_c_decode_file",
    #":example_c_encode_file",
    #":example_cpp_decode_file",
    #":example_cpp_encode_file",
  ]
}