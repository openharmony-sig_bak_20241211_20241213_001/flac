# flac

## 简介
> flac是速度最快、支持最广泛的无损音频编解码器。

![](screenshot/result.png)

## 下载安装
直接在OpenHarmony-SIG仓中搜索flac并下载。

## 使用说明
以OpenHarmony 3.1 Beta的rk3568版本为例

1. 将下载的flac库代码存在以下路径：./third_party/flac

2. 修改添加依赖的编译脚本，路径：/developtools/bytrace_standard/ohos.build

```

{
  "subsystem": "developtools",
  "parts": {
    "bytrace_standard": {
      "module_list": [
        "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
        "//developtools/bytrace_standard/bin:bytrace_target",
        "//developtools/bytrace_standard/bin:bytrace.cfg",
        "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
        "//third_party/libflac:libflac_targets",
        "//third_party/libogg:libogg"
      ],
      "inner_kits": [
        {
          "type": "so",
          "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
          "header": {
            "header_files": [
              "bytrace.h"
            ],
            "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
          }
        }
      ],
      "test_list": [
        "//developtools/bytrace_standard/bin/test:unittest"
      ]
    }
  }
}

```

3. 编译：./build.sh --product-name rk3568 --ccache

4. 生成库文件和一些可执行测试文件，路径：out/rk3568/developtools/profiler

## 接口说明
1. 创建新的链实例：
   `FLAC__metadata_chain_new()`
2. 删除链指向的对象：
   `FLAC__metadata_chain_delete()`
3. 获取链的当前状态：
   `FLAC__metadata_chain_status()`
4. 将FLAC文件中的所有元数据读取到链中：
   `FLAC__metadata_chain_read()`
5. 将Ogg FLAC文件中的所有元数据读取到链中：
   `FLAC__metadata_chain_read_ogg()`
6. 通过I/O回调将FLAC流中的所有元数据读取到链中：
   `FLAC__metadata_chain_read_with_callbacks()`
7. 通过I/O回调将Ogg FLAC流中的所有元数据读取到链中：
   `FLAC__metadata_chain_read_ogg_with_callbacks()`
8. 检查写入给定链是否需要使用临时文件，或者是否可以就地写入：
   `FLAC__metadata_chain_check_if_tempfile_needed()`
9. 将所有元数据写入FLAC文件：
   `FLAC__metadata_chain_write()`
10. 通过回调将所有元数据写入FLAC流：
      `FLAC__metadata_chain_write_with_callbacks()`
11. 将相邻的填充块合并为单个块：
      `FLAC__metadata_chain_merge_padding()`
12. 将元数据上的所有填充块移动到末尾，然后将它们合并到单个块中：
      `FLAC__metadata_chain_sort_padding()`
13. 初始化迭代器以指向给定链中的第一个元数据块：
      `FLAC__metadata_iterator_init()`
14. 将迭代器向前移动一个元数据块：
      `FLAC__metadata_iterator_next()`
15. 将迭代器向后移动一个元数据块：
      `FLAC__metadata_iterator_prev()`
16. 获取当前位置的元数据块：
      `FLAC__metadata_iterator_get_block()`
17. 将元数据块设置在当前位置，替换现有块：
      `FLAC__metadata_iterator_set_block()`
18. 从链中删除当前块：
      `FLAC__metadata_iterator_delete_block()`
19. 在当前块之前插入新块：
      `FLAC__metadata_iterator_insert_block_before()`
20. 在当前块之后插入新块：
      `FLAC__metadata_iterator_insert_block_after()`

## 约束与限制

在下述版本验证通过：

DevEco Studio 版本：3.1 Beta1(3.1.0.200)，SDK:API9 Beta5(3.2.10.6)

## 目录结构
````
|---- flac
|     |---- bulid                    #编译文件
|     |---- cmake                    #cmake文件
|     |---- doc                      #HTML文档
|     |---- examples                 #示例程序
|     |---- include                  #公共头文件
|     |---- man                      #“flac”和“metaflac”的手册页
|     |---- src
|           |---- flac               #FLAC编码器/解码器命令行
|           |---- libFLAC++          #免费无损音频编解码器库
|           |---- libFLAC            #免费无损音频编解码器库
|           |---- metaflac           #FLAC元数据编辑器命令行
|           |---- plugin_common      #通用插件
|           |---- plugin_xmms        #XMMS FLAC输入插件
|           |---- share              #工具和插件
|           |---- utils
|                 |---- flacdiff     #显示两个FLAC流不同的位置
|                 |---- flactimer    #运行命令并打印计时信息
|     |---- tests                    #测试脚本文件
|     |---- README.md                #安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/flac/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/flac/pulls) 。

## 开源协议
本项目基于 [FDL License](https://gitee.com/openharmony-sig/flac/blob/master/COPYING.FDL)、[GPL License](https://gitee.com/openharmony-sig/flac/blob/master/COPYING.GPL)、[LGPL License](https://gitee.com/openharmony-sig/flac/blob/master/COPYING.LGPL)、[BSD License](https://gitee.com/openharmony-sig/flac/blob/master/COPYING.Xiph) ，请自由地享受和参与开源。